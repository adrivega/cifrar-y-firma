import java.security.*;
import java.util.Scanner;
import javax.crypto.Cipher;

public class Main {
    public static void main(String[] args) {

        GeneradorClaves generador = new GeneradorClaves("RSA");
        generador.generar();

        CifradoYFirma cifrador = new CifradoYFirma("RSA", generador.publicKey, generador.privateKey);
        cifrador.cifrarMensaje();
        cifrador.firmarMensaje();

        DescifradoYFirma descifrador = new DescifradoYFirma("RSA", generador.publicKey, generador.privateKey, cifrador.getMensajeCifrado(), cifrador.getFirma());
        String mensajeDescifrado = descifrador.descifrarMensaje();
        boolean esValido = descifrador.verificarFirma(mensajeDescifrado);

        System.out.println("Mensaje descifrado: " + mensajeDescifrado);
        System.out.println("La firma es válida: " + esValido);

    }
}
