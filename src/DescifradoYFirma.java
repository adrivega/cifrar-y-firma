import java.security.*;
import java.util.Scanner;
import javax.crypto.Cipher;


public class DescifradoYFirma {
    private String algoritmo;
    private PublicKey publicKey;
    private PrivateKey privateKey;
    private byte[] mensajeCifrado;
    private byte[] firma;

    public DescifradoYFirma(String algoritmo, PublicKey publicKey, PrivateKey privateKey, byte[] mensajeCifrado, byte[] firma) {
        this.algoritmo = algoritmo;
        this.publicKey = publicKey;
        this.privateKey = privateKey;
        this.mensajeCifrado = mensajeCifrado;
        this.firma = firma;
    }

    public String descifrarMensaje() {
        try {

            Cipher cipher = Cipher.getInstance(algoritmo);
            cipher.init(Cipher.DECRYPT_MODE, publicKey);
            return new String(cipher.doFinal(mensajeCifrado));

        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    public boolean verificarFirma(String mensajeDescifrado) {
        try {

            Signature publicSignature = Signature.getInstance("SHA256withRSA");
            publicSignature.initVerify(publicKey);
            publicSignature.update(mensajeDescifrado.getBytes());

            return publicSignature.verify(firma);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }
}
