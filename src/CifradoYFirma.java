import java.security.*;
import java.util.Scanner;
import javax.crypto.Cipher;
public class CifradoYFirma {
    private String algoritmo;
    private String mensaje;
    private PublicKey publicKey;
    private PrivateKey privateKey;

    private byte[] mensajeCifrado;
    private byte[] firma;

    public CifradoYFirma(String algoritmo, PublicKey publicKey, PrivateKey privateKey) {
        this.algoritmo = algoritmo;
        this.publicKey = publicKey;
        this.privateKey = privateKey;
    }

    public void cifrarMensaje() {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Introduce el mensaje a cifrar:");
        this.mensaje = scanner.nextLine();

        try {

            Cipher cipher = Cipher.getInstance(algoritmo);
            cipher.init(Cipher.ENCRYPT_MODE, privateKey);

            this.mensajeCifrado = cipher.doFinal(mensaje.getBytes());

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void firmarMensaje() {
        try {

            Signature privateSignature = Signature.getInstance("SHA256withRSA");
            privateSignature.initSign(privateKey);
            privateSignature.update(mensaje.getBytes());

            this.firma = privateSignature.sign();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public byte[] getFirma() {
        return firma;
    }

    public byte[] getMensajeCifrado() {
        return mensajeCifrado;
    }

}

